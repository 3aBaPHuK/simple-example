<?php

namespace Guayaquil;

use SoapClient;
use stdClass;

class GuayaquilApi
{
    /**
     * @var SoapClient
     */
    private $client;
    /**
     * @var string
     */
    private $login;
    /**
     * @var string
     */
    private $password;

    public function __construct(string $login, string $password)
    {
        $this->client = new SoapClient('http://ws.laximo.net/wsdl/Laximo.OEM_Login.xml');
        $this->login = $login;
        $this->password = $password;
    }

    public function getCatalogList(): stdClass
    {
        $request = 'ListCatalogs:Locale=en_US|ssd=';

        return $this->client->QueryDataLogin([
            'request' => $request,
            'login' => $this->login,
            'hmac' => md5($request.$this->password)
        ]);
    }
}